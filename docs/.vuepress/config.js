module.exports = {
    dest: 'public',
    host: 'localhost',
    port: '8080',
    locales: {
        '/': {
            lang: 'zh-CN',
            title: '数据科学基础',
            description: 'Spring 2019'
        }
    },
    head: [
        ['link', {
            rel: 'stylesheet',
            href: '//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css'
        }]
    ],
    themeConfig: {
        repo: 'https://gitlab.com/njufds/njufds.gitlab.io.git',
        displayAllHeaders: true,
        nav: [{
            text: '大纲',
            link: '/catalog/'
        }, {
            text: '资源',
            link: '/resource/'
        }, {
            text: '任务',
            link: '/assignments/'
        }, {
            text: '人员',
            link: '/people/'
        }],
        sidebar: {
            '/catalog/': genSidebarConfig('Catalog'),
            '/assignments/': genSidebarConfig('Assignment'),
            '/meeting/': genSidebarConfig('Meeting')
        },
        algolia: {
            apiKey: 'acfb1b0291a2bb42f0cd21bae2b1d6b9',
            indexName: 'njufds'
        }
    }
}

function genSidebarConfig(title) {
    if (title === 'Assignment') {
        return [{
            title: '任务',
            collapsable: false,
            children: [
                '', 'week1'
            ]
        }]
    }
    if (title === 'Catalog') {
        return [{
            title: '目录',
            collapsable: false,
            children: [
                '',
                'concept',
                'data',
                'statistics',
                'predictive'
            ]
        }]
    }
    if (title === 'Meeting') {
        return [{
            title: '报告',
            collapsable: false,
            children: [
                '',
                'fds_pre_0910',
                'note_0910'
            ]
        }]
    }
}
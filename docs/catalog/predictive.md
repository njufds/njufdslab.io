# 11~15 预测分析

本章主要讲数据分析中的第三步预测分析，用预测性分析而不是机器学习感觉更符合数据分析的过程。同时涉及最后一步最优决策中的一部分，即模型评估和选择。

### 课时安排

许多课程在机器学习方面安排了非常多的课时，考虑到我们之后还会有机器学习相关的课程，并且统计分析要用很多节课，所以本章计划了5节课，其中有两节还是为以往统计分析课中的回归分析准备的。

NLP基础、分类和模型选择和评估则各占一节课。

## NLP基础

本节简要介绍 NLP 的一些基础知识。

### 知识点

<Keywords :cata="[3, 0]"></Keywords>

### 实践点

1. Python 中英文 NLP 库
2. Python 实现词袋模型和 TF-IDF
3. NLP 各种技术（如 [语言处理基础技术](http://ai.baidu.com/tech/nlp)）

## 回归分析

回归分析是每个数据科学课程都会讲的内容。

### 知识点

<Keywords :cata="[3, 1]"></Keywords>

### 实践点

1. Python 线性回归分析（从一元到多元，如 [Data Science Simplified Part 4~5](https://www.datasciencecentral.com/profiles/blog/list?user=06aw9fgllidx6)）
2. Python 非线性回归分析（如 [Robust nonlinear regression in scipy](https://scipy-cookbook.readthedocs.io/items/robust_regression.html)）

## 分类

分类也是都会讲的内容，有些预测性分析最后是形成分类。

### 知识点

<Keywords :cata="[3, 2]"></Keywords>

### 实践点

1. 逻辑回归手写识别
2. 朴素贝叶斯文本分类

## 模型评估和选择

模型评估和选择也是每个数据科学课程都会讲的内容，毕竟做数据分析最终都是要形成数据科学产品或是产生分析报告。自然要对模型进行评估，并选择优秀的模型。

### 知识点

<Keywords :cata="[3, 3]"></Keywords>

### 实践点

1. Python 模型评估（如 [机器学习-浅谈模型评估的方法和指标](https://www.jianshu.com/p/498ea0d8017d/)）
2. Python 交叉验证
3. Python 模型选择（如 [简明数据科学 第六部分：模型选择方法](http://blog.talkingdata.com/?p=6027)）


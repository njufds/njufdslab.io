# 2~3 数据认知

本章将从历史和科学的角度来介绍数据。

## 1. 数据是什么

### 1.1 「Data」 单词的来源

https://idalab.de/blog/data-science/what-do-we-mean-by-data

数据是一个抽象的、无量的、未被识别的大量数字（数字编码），对我们的生活产生了巨大的影响。「数据」这个词的起源是什么？它的含义是什么？

现在大多数人都知道 data 是 datum 的复数形式。在拉丁文中， data 则是 dare 的过去分词， dare 在拉丁语中的意思是给予 ，因此 data 在拉丁文中的意思是被给予。

「data」这个词最早的科学用法来自欧几里得的 data 这本书中。

> 这篇文章有点难翻译，也不是太需要，主要是讲 数据 这个词的由来。先跳过。。。

### 1.2 数据的定义 

#### Wikipedia ZH

https://zh.wikipedia.org/wiki/数据

**数据**（英语：data），是指未经过处理的原始记录。一般而言，数据缺乏组织及分类，**无法明确的表达事物代表的意****义，它可能是一堆的杂志、一大叠的报纸、数种的开会记录或是整本病人的病历纪录。数据描述[事物](https://zh.wikipedia.org/wiki/%E4%BA%8B%E7%89%A9)的[符号](https://zh.wikipedia.org/wiki/%E7%AC%A6%E5%8F%B7)记录，是可定义为[意义](https://zh.wikipedia.org/w/index.php?title=%E6%84%8F%E4%B9%89&action=edit&redlink=1)的[实体](https://zh.wikipedia.org/wiki/%E5%AF%A6%E9%AB%94)，涉及事物的[存在形式](https://zh.wikipedia.org/w/index.php?title=%E5%AD%98%E5%9C%A8%E5%BD%A2%E5%BC%8F&action=edit&redlink=1)。是关于事件之一组[离散](https://zh.wikipedia.org/wiki/%E9%9B%A2%E6%95%A3)且客观的[事实](https://zh.wikipedia.org/wiki/%E4%BA%8B%E5%AF%A6)描述，是构成[消息](https://zh.wikipedia.org/wiki/%E8%A8%8A%E6%81%AF)和[知识](https://zh.wikipedia.org/wiki/%E7%9F%A5%E8%AF%86)的原始材料。

#### Wikipedia EN

https://en.wikipedia.org/wiki/Data

Data as a general [concept](https://en.wikipedia.org/wiki/Concept) refers to the fact that some existing [information](https://en.wikipedia.org/wiki/Information) or [knowledge](https://en.wikipedia.org/wiki/Knowledge) is [*represented*](https://en.wikipedia.org/wiki/Knowledge_representation_and_reasoning) or [*coded*](https://en.wikipedia.org/wiki/Code) in some form suitable for better usage or [processing](https://en.wikipedia.org/wiki/Data_processing). [*Raw data*](https://en.wikipedia.org/wiki/Raw_data) ("unprocessed data") is a collection of [numbers](https://en.wikipedia.org/wiki/Number) or [characters](https://en.wikipedia.org/wiki/Character_(computing)) before it has been "cleaned" and corrected by researchers.

#### Wiktionary

https://en.wiktionary.org/wiki/data

Wiktionary 将数据定义为记录器和读取器都理解的某种尺度上的测量，是直接观察得到的事实。

#### Computerhope

<https://www.computerhope.com/jargon/d/data.htm>

一般来说数据是一组用于某种目的（通常是分析）的已被收集和翻译的 Characters。可以有各种形式。数据如果没有被放入某个特定的Context中，它对计算机和人类来说没有任何作用。

#### Wikibooks

> 下面内容是 Wikibooks 的意思，只供参考

<https://en.wikibooks.org/wiki/Data_Science:_An_Introduction/Definitions_of_Data>

「数据」是一个表示测量集合的通用词。「数据点」指的是数据的个别实例，数据点可以是多种「数据类型」，例如数字、文本或日期时间。当我们以类似的格式收集关于相似对象的数据时，我们将这些数据点归纳成一种「变量」，这些数据点则是变量的值。（这里变量用对象的属性似乎更好）。「数据集」是一组结构良好的数据点。

wikibooks 定义数据是以记录器（比如机器）和读取器（比如人）都能理解的方式测量和传达的观测值。你作为人不是数据，但是关于你的观察记录是数据。比如你写的名字，你说出的你的名字，你的脸的照片等。

##### 数据点 Data Point

**Data is or Data are ?**

data和datum谁是单数形式，谁是复数形式？ - 郑哲东的回答 - 知乎
https://www.zhihu.com/question/57380133/answer/165044645

Datum 是 Data 的单数形式，但现在单数的 Data 开始被接受。

数据是测量集合，那么单个测量值是 datum 吗？Wikipedia 将单个测量值定义为「数据点」而不是 datum。

当我们说「data」时，表示的是一组聚合的数据点。

##### 数据集

**A set of data ?**

数据是一组聚合的数据点，那么数据集又是什么呢？一组聚合的「多组聚合的数据点」？

数据是一种比较泛的通用词。数据集不是建立在「数据」之上的，而是建立在「数据点」之上。我们将一组使用类似方式格式化的、在类似对象上观察的数据点，归纳为「变量」，数据点则是变量的值。数据集则是一组结构良好的数据点。

比如进入图书馆记录了你的信息，单次的记录就是一个数据点，我们通过观测发现进入图书馆的人都有一些共同的特征，比如都有名字，都有年龄，那么名字和年龄就可以被归纳为一种确定的变量（人的属性），拥有这些特定变量的一组数据点构成了数据集。

简单来说，观测得到的都能称为数据，数据点事数据的个别实例，数据集是这些实例的集合。

**一些经典的数据集**

https://en.wikipedia.org/wiki/Data_set

##### 数据类型

数据有不同的形式，文本、数字、图像、音频和视频。数据工作者会非常小心地专门定义不同的数据类型，因为在不同数据类型上的操作符的含义是不一样的。比如数字上的 1 + 1 = 2，在文本上呢？（在图像上呢？）。

- 数学中的数据类型
- 统计中的数据类型
- 计算机科学中的数据类型
- 图像的数据类型数据

### 1.3 数据、信息和知识

> 信息是一种描述，而知识是一种推断（预测）。比如7月份的天气数据中我们可以获得7月份的平均气温，最高温和最低温，这是信息。再从多年的7月平均气温变化情况，我们可以推断之后每天7月份的大概气温。

#### 例子

[以体温为例讲数据、信息和知识之间的关系](https://wenku.baidu.com/view/85b88eef8ad63186bceb19e8b8f67c1cfad6eef9.html)

[以天气为例](https://www.sohu.com/a/193915062_737873)

## 2. 数据的表示和存储

### 2.1 数据的产生

从人工的数据到机器记录的数据：

- 数据的形式在发生变化
- 数据的容量在发生变化

### 2.2 以前的存储方式

竹简、纸、磁带、碟片

### 2.3 计算机如何表示和存储数据？

> 有什么特点，需要以什么方式进行表示？

计算机是一个二进制系统，无论是什么样的数据类型，最终都要转为二进制进行存储。这一点就与之前对数据的定义有点违背了—人类是难以理解这种「数据」的。因此需要一些方式来表示数据：

http://csfieldguide.org.nz/en/chapters/data-representation.html

- 负数：补码
- 浮点数：二进制科学计数法
- 字符：ASCII/Unicode 编码
- 图像：位图和矢量图

这些只是基本的数据类型，但是数据除了有类型，还有结构在发展。

- 结构化的数据：表格、关系数据库
- 半结构化的数据：比如员工的简历，转为结构化？--> 新的格式如 XML 和 JSON
- 非结构化数据：比如日志 --> NoSQL

数据除了结构在发展，容量也在变化

数据容量的发展带来的数据表示和存储方式的变革。

https://wenku.baidu.com/view/db52f013ec630b1c59eef8c75fbfc77da26997a9.html

- 集中式存储
- 分布式存储

## 3. 现在的数据

### 3.1 容量

### 3.2 特征

### 3.3 质量

## 4. 探索数据

对数据进行初步的探索

###4.1 数据源

数据的来源

### 4.2 数据预处理

### 4.3 数据可视化

### 4.4 简单数据统计

## 5. 数据的应用和影响

### 美国的数据历史之人口普查

https://wenku.baidu.com/view/19bb434766ec102de2bd960590c69ec3d4bbdb0b.html

数据表示的是过去，但表达的是未来，所以观察数据需要有历史观。

该课件讲数据在美国历史的应用与影响

小数据：源于测量

- 初数时代：用数据分权、**人口普查**、数据意识、数学教育
- 镀金时代：西顿制表机 、用于人口普查的第一台商用计算机
- 抽样时代：大不代表更准，盖洛普、《乱世佳人》

大数据 = 传统的小数据 + 现代的大记录

- 开放时代：**信息公开**，数据成为重要的生产资料和创新资源，LEHD项目
- 大数据时代：通往计算型的智能社会

### A History of Data Collection, Storage, and Analysis

https://www.gutcheckit.com/blog/a-history-of-data/

随着时间的推移，数据发生了巨大的变化，特别是数量、收集、类型和分析。数据也随着影响每个人和每个行业发展到今天。了解其历史非常重要，尤其是当数据的下一次转变正在发生时。

> 需要补充的内容：下一次转变是啥？

##### 数据早期的形式

早期数据的第一种形式是标记和刻度标记，收集这些资料是为了跟踪或记录库存。

工具：当这些数据需要一些统计信息时，算盘被发明出来计算这些记录。

纸被发明出来后，人们可以通过更多的文本格式比如表格来记录数据，也可以通过图像来表示数据，比如地图。接着天文学和一些与时间有关的科学研究产生了时序数据等

> 上面说的很乱，总之就是不同类型的数据都有不同的起源，也有着不同的工具来收集、分析和存储。
>
> 这点可以多扩展些

总之，在不同的数据历史时期，都有不同的产生收集、分析和存储数据的工具。

##### 数据进化

随着社会的进步，产生了更多的数据需求。 例如，在1800年代，**人口普查**开始发生 - 这是迄今为止最大的数据来源之一。 由于人口普查正在收集越来越多的数据点，因此需要一个更有效的数据收集和分析过程。 事实上，如果没有它，编制和分析人口普查数据将无法完成，直到接下来的人口普查出现时。

> 未完待续。。。

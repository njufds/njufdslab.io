# 4~10 统计分析

获得了可用于分析的数据后，接下来就是考虑如何进行数据分析。数据分析如之前所说，分成两个部分，一是统计分析，二是预测分析。

而本章所述的统计分析又可以大体分为两个部分，一是描述性统计，二是统计推断。

统计分析主要继承之前统计课的内容，区别是

1. 将原先概率论和随机变量合为一节课
2. 将参数和非参假设检验合为一节课
3. 将回归分析放到下一章预测分析中
4. 增加一点因果关系的内容

### 课时安排

本章内容是与其他课程的主要差异所在，大部分课程本章涉及的内容一般只有3到4个课时。本章为下面每一节内容安排一节课，共7节。

## 描述性统计

描述性统计包括其他课程中的数据可视化和探索性数据分析的内容。

### 知识点

<Keywords :cata="[2, 0]"></Keywords>

### 实践点

1. Python 数据统计 Numpy 和 Scipy
2. Python 数据可视化 Matplotlib

## 概率论基础

概率论基础主要讲本课程之后需要用到概率论知识。

### 知识点

<Keywords :cata="[2, 1]"></Keywords>

### 实践点

1. Python 实现概率论基础知识
2. Python 中的随机数和概率分布（如 [认识概率，用python模拟掷硬币](https://www.jianshu.com/p/0f985e5f4fcb)）

## 统计基础

统计基础即原第五章 统计量及其抽样分布。本来的想法是将其放到参数估计和假设检验之中，因为抽样、分布和中心极限定理在下面两节课中都会用到。

### 知识点

<Keywords :cata="[2, 2]"></Keywords>

### 实践点

1. Python 实现统计基本知识
2. Python 实现抽样和抽样分布（如 [数据探索之统计分布](https://www.jianshu.com/p/8a0479f55b21)）
3. Python实现中心极限定理

## 参数估计

参数估计实际上在其他课程中是以 Bootstrapping、置信区间和试验性设计等关键词出现。

### 知识点

<Keywords :cata="[2, 3]"></Keywords>

### 实践点

1. Python 实现点估计（最大似然估计）
2. Python 实现区间估计（如 [用python学统计之区间估计](http://yz.jiangnan.edu.cn/sszs/sszsml/114451.shtml)）

## 假设检验

假设检验基本上是每个课程都讲的内容。

### 知识点

<Keywords :cata="[2, 4]"></Keywords>

### 实践点

1. Python 实现各类假设检验（如 [数据探索之假设检验](https://www.jianshu.com/p/99fd951a0b8b)）

## 方差分析

方差分析则很少课程讲到。

### 知识点

<Keywords :cata="[2, 5]"></Keywords>

### 实践点

1. Python 实现单因素方差分析（如 [Four ways to conduct one-way ANOVA with Python](https://www.marsja.se/four-ways-to-conduct-one-way-anovas-using-python/)）
2. Python 实现双因素方差分析（如 [Three ways to do a two-way ANOVA with Python](http://www.pybloggers.com/2016/03/three-ways-to-do-a-two-way-anova-with-python/)）
3. 正交实验法设计测试用例（如 [用正交实验法设计测试用例](https://blog.csdn.net/fangnannanf/article/details/52813498)）

## 相关性和因果关系

许多课程考虑到了因果关系，也挺适合在讲相关性分析中提到。

### 知识点

<Keywords :cata="[2, 6]"></Keywords>

### 实践点

1. Python 数据相关性分析